.. Workshop - QompliGIS documentation master file, created by
   sphinx-quickstart on Wed Jan 12 16:07:06 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Workshop - QompliGIS
====================

.. toctree::
   :maxdepth: 2
   :caption: Sommaire:

   fr/00_workshop.rst
   fr/01_qompligis_file_formats.rst
   fr/02_data.rst
   fr/03_create_dataset.rst
   fr/04_create_configuration.rst
   fr/05_verification.rst
   fr/06_enhancement.rst


* :ref:`genindex`
* :ref:`search`

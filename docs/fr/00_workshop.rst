Atelier QompliGIS
=================

Introduction
------------

Présentation de l'atelier
-------------------------

Présentation de QompliGIS
.........................

Ce plugin a pour objectif de proposer un moyen simple de vérifier si la structure
d’un jeu de données est conforme avec la structure d’un jeu de données de rérérence.

Les formats supportés sont :

 * geopackage
 * dossier de shapefile(s)
 * DXF


Comment ça marche ?
+++++++++++++++++++

Prérequis
_________

Un jeu de données de référence est requis.

1. Processus

    * Créer un fichier de configuration de la conformité dans lequel peuvent
      être choisis plusieurs réglages

    * Vérification du SCR

    * Présence de trous et de courbes

    * Champs

    * etc.

2. Utiliser ce fichier de configuration et un jeu de données à vérifier pour
   lancer la vérification de conformité

3. Analyser le rapport


Installation
------------

Le plugin est installable depuis le gestionnaire d'extension de QGIS.

.. image:: ../static/fr/installation_plugin_qompligis.png


Jeu de donnée initial
---------------------

Où est stockée ma donnée ?
..........................

- PostgreSQL
- GeoPackage
- Shapefile
- Autres ?

Rappel sur les formats de fichiers
++++++++++++++++++++++++++++++++++

La réalisation d'un cahier des charges impose un format interopérable sur les différents logiciels.
Malheureusement, il n'est parfois pas possible de satisfaire tout le monde.
On peut avoir besoin de réaliser une version "simple" de la donnée afin de correspondre aux différents formats et logiciels.

Certains proposent plusieurs formats possibles et réalisent une conversion après vérification. C'est ce que nous allons faire.

Le plugin sait lire le GeoPackage, le ShapeFile, et le DXF. D'autres formats SIG pourraient être intégrés mais n'ont pas encore été développés.

GeoPackage :
    - Une base de données embarquée (sqlite)
    - Standardisé (OGC)
    - Format par défaut de QGIS depuis la version 3
    - Peut contenir un projet QGIS
    - Parfait pour l'échange (sur QGIS)

Shapefile :
    - Le format SIG standard de facto
    - Utilisable par des outils de DAO
    - Mais à des limitations sur la structuration des champs et des géométries
    - A besoin de beaucoup de fichiers (.shp, .dbf, .shx, .prj, .etc)

DXF :
    - Format standard et ouvert de la DAO
    - Utilisable dans QGIS, mais avec des différences
    - Logique différente, il faut adapter le DXF au contexte SIG

Exporter PostgreSQL vers un format standard
+++++++++++++++++++++++++++++++++++++++++++

.. note::

   PARTIE OPTIONNELLE CAR NÉCESSITE UNE BASE POSTGRESQL

PostgreSQL n'est pas supporté par défaut par le plugin, bien que ce soit la meilleure base pour gérer les données SIG avec PostGIS.

Bien que certains puissent échanger des dumps de la base, il est rare que les auteurs des plans soient en
mesure de produire des plans directement au format PostgreSQL. Pour cela, nous préconisons un export vers un format d'échange supporté.

PostgreSQL vers du Shapefile
****************************

QGIS vous permet de convertir vos fichiers dans différents formats dont le
GeoPackage et le Shapefile. Plusieurs moyens peuvent être mis en œuvre pour y parvenir.
Pour cela, vous pouvez :

* enregistrer les fichiers en utilisant le menu "Exporter->Sauvegarder les entités sous"

* utiliser la commande de conversion
  `ogr2ogr. <https://gdal.org/programs/ogr2ogr.html>`_
  Celle-ci étant présente dans l'outil de traitement de QGIS et possède
  l'avantage de pouvoir spécifier des options de conversions.
  C'est la méthode que nous préconisons.

Utilisation de ogr2ogr via QGIS
_______________________________

* ouvrez l'outil Conversion de format dans la boîte à outils de traitements.

* sélectionnez vos tables PostgreSQL en entrée et indiquez vos ShapeFiles en sortie.


.. note::

   Pour plus d'informations nous vous laissons vous reporter au manuel de ogr2ogr.

PostgreSQL vers du GeoPackage : Utilisation d'export DB Mapper
**************************************************************

Comme pour n'importe quel format vous pouvez utiliser l'outil de
conversion de QGIS.

Néanmoins, il n'est pas forcément adapté à votre usage.
Pour celà, il existe plusieurs plugins pour la conversion de PostgreSQL vers du
GeoPackage. Notre préconisation porte sur le plugin ExportDBMapper que nous avons
développé.
À l'inverse d'autres plugins, celui-ci a la particularité de pouvoir s'intégrer
totalement dans une chaîne de traitement QGIS. Ce plugin est en quelque sorte
une fusion du meilleur de QConsolidate et d'ogr2ogr.

.. note::

   La documentation du projet est disponible à cette
   `page. <https://oslandia.gitlab.io/qgis/exportdbmapper/fr_guide/1-usage.html>`_

Étape d'export
______________

Vous définissez l'export de vos données PSQL en GPKG et vous obtenez un fichier
YAML qui vous sera utile pour réaliser cette conversion avec la possibilité en
plus d'avoir un fichier de projet QGIS.

Vous pouvez ouvrir le projet QGIS et créer votre vérification comme indiqué dans
l'étape suivante.

Grâce au projet QGIS et au(x) GeoPackage vous aurez un ensemble cohérent à
fournir à votre prestataire réalisant les plans.

Étape d'import
______________

Pour les personnes désirant recharger automatiquement la donnée vérifiée
dans une base PostgreSQL il vous suffira de créer le fichier de configuration
YAML de la phase d'export.

.. note::

   QompliGIS est une "brique" générique qui s'intègre pleinement dans
   la chaîne de traitement de QGIS. Libre à vous de configurer les post- et pré-
   traitements pour l'automatisation de l'intégration de vos données.
   Si vous souhaitez bénéficier d'une assiste ou d'une formation sur ces sujets,
   `contactez-nous. <mailto:infos@oslandia.com?subject=QompliGIS%20plugin>`_

PostgreSQL vers DXF
*******************

QGIS propose plusieurs outils pour réaliser une conversion des données et du
canevas en DXF. Le plus simple est selon-nous d'utiliser « Exporter les
couches en DXF » de la boîte à outils des traîtements

.. image:: ../static/fr/configuration_export_psql_dxf.png

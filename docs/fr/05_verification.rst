Vérification de la conformité
=============================

Utiliser l’icône |checkConfig| pour ouvrir l’assistant de vérification de conformité.



Choisir le fichier de configuration de la conformité
----------------------------------------------------

.. image:: ../static/fr/verification_1.png

Sélectionner le fichier (ou dossier dans le cas de shapefiles) de données
-------------------------------------------------------------------------

.. image:: ../static/fr/verification_2.png

Export du rapport
-----------------
La page de rapport montre les étapes de vérifications en direct

.. image:: ../static/fr/verification_3.png

Le rapport est enregistré en choisissant un format et une destination sur la
dernière page de l’assistant.

Les formats supportés sont :

* HTML

* Markdown

.. image:: ../static/fr/verification_4.png

Utilisation des traitements
---------------------------

Afin d'automatiser la vérification et la création du rapport dans QGIS, un outil
a été ajouté dans la boite des traitements. Il vous permet d'enchaîner la
vérification avec une autre action : vérification complémentaire, intégration de
la donnée, envoi d'un mail, etc.

Il s'utilise de la même façon, mais avec seulement une fenêtre :

.. image:: ../static/fr/verification_5.png

.. |checkConfig| image:: ../static/checkCompliance.svg
